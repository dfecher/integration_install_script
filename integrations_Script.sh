#!/bin/bash

#List All Available Integrations from github
function list_available_integrations(){
  echo

  curl "https://api.github.com/search/repositories?q=topic:polarity-integration&org:polarityio&per_page=1000" | grep \"clone_url\" | awk -F "/" '{print $NF}' | awk -F "." '{print $1}' | sort

}

#install single or multiple integration
function install_integrations(){
echo
echo "Enter integration name(s) you want to install (Comma seperated)-e.g.arin,google-maps)"
read -r integNames

#Change to integrations directory
cd /app/polarity-server/integrations/

for i in $(echo $integNames | sed "s/,/ /g")
do
#check if integration exists and clone it
if curl --output /dev/null --silent --head --fail "https://github.com/polarityio/${i}"; then
  #Clone integration
  git clone https://github.com/polarityio/${i}

  #Install dependencies
  cd /app/polarity-server/integrations/${i};npm install;cd ..;

  #Set permissions
  chown -R polarityd:polarityd /app/polarity-server/integrations;
  echo ${i} "integration installed and permissions set"
else
  printf '%s\n' "${i} DOES NOT EXIST!"
fi
echo
done

#Restart services
systemctl restart polarityd
echo "polarityd service restarted"
echo "ENJOY YOUR POLARITY INTEGRATION(s)!"
}

#install ALL integrations
function install_all_integrations(){
  #Change to integrations directory
  cd /app/polarity-server/integrations/

  #Get full list of current integrations
  curl "https://api.github.com/search/repositories?q=topic:polarity-integration&org:polarityio&per_page=1000" | grep \"clone_url\" | awk '{print $2}' | awk 'END{print "There are over " NR " integrations."}'
  read -p "Would you like to install ALL integrations? (Y/n): " -n 1 -r
  echo    #(optional) move to a new line
  if [[ $REPLY =~ ^[Yy]$ ]]
  then

  #Get full list of current integrations and clone them
  curl "https://api.github.com/search/repositories?q=topic:polarity-integration&org:polarityio&per_page=1000" | grep \"clone_url\" | awk '{print $2}' | sed -e 's/"//g' -e 's/,//g' | xargs -n1 git clone

  #Install dependencies
  for i in `ls /app/polarity-server/integrations`;do cd /app/polarity-server/integrations/$i;npm install;done

  #Set permissions
  chown -R polarityd:polarityd /app/polarity-server/integrations

  #Restart services
  systemctl restart polarityd
  echo "All Integrations Installed"

  fi
}

#Update Single/Multiple Installed Integrations
function update_integrations(){
  echo
  ls /app/polarity-server/integrations/
  echo
  echo "Enter integration name(s) you want to update (Comma seperated)-e.g.arin,google-maps): "
  read -r integNames

  for i in $(echo $integNames | sed "s/,/ /g")
  do

  #Update Integration
  cd /app/polarity-server/integrations/$i;git pull;cd ..;
  done

  #Set permissions
  #chown -R polarityd:polarityd /app/polarity-server/integrations

  #Restart services
  systemctl restart polarityd
  echo "UPDATE(S) DONE!"

}
#Update ALL Installed Integrations
function update_all_integrations(){
  echo
  echo "Updating All Installed Integrations"
  #Updating integrations
  for i in `ls /app/polarity-server/integrations`;do cd /app/polarity-server/integrations/$i;git pull;done

  #Set permissions
  chown -R polarityd:polarityd /app/polarity-server/integrations

  #Restart services
  systemctl restart polarityd
  echo "UPDATES DONE!"

}

#List installed integrations
function list_installed_integrations(){
echo
ls /app/polarity-server/integrations/
echo
}

#Remove Installed integration
function remove_installed_integrations(){
echo
ls /app/polarity-server/integrations/
echo
echo "Enter integration name(s) you want to remove (Comma seperated)-e.g.arin,google-maps): "
read -r integNames

#Change to integrations directory
cd /app/polarity-server/integrations/

for i in $(echo $integNames | sed "s/,/ /g")
do
  #Remove integration
  rm -rf ${i}
  echo ${i} "integration removed"
done

#Restart services
systemctl restart polarityd
echo "Polarity Service restarted"
echo
echo "ALL DONE!"
}

#Remove ALL Installed integration
function remove_all_integrations(){
echo
ls /app/polarity-server/integrations/
echo
read -p "Are sure you want to remove (ALL) integrations? (Y/n): " -n 1 -r
echo    #(optional) move to a new line
if [[ $REPLY =~ ^[Yy]$ ]]
then

#remove ALL integrations
rm -rf /app/polarity-server/integrations/*

#Restart services
systemctl restart polarityd
echo "All integratations have been removed and the Polarity Service restarted"
echo "HAVE A NICE DAY!"

fi
}

case ${1} in
  -list-available-integrations|-L)
    list_available_integrations
    ;;
  -install-integrations|-i)
    install_integrations
    ;;
  -install-all-integrations|-a)
    install_all_integrations
    ;;
  -update-integrations|-u)
    update_integrations
    ;;
  -update-all-integrations|-U)
    update_all_integrations
    ;;
  -list-installed-integrations|-l)
    list_installed_integrations
    ;;
  -remove-installed-integrations|-r)
    remove_installed_integrations
    ;;
  -remove-all-integrations|-e)
    remove_all_integrations
    ;;
 *)
   echo
   echo " Usage...: ${0} [option]"
   echo ' Options.:'
   echo '           -L = List (ALL) Integrations Available For Install'
   echo '           -i = Install Single/Multiple Integration(s)'
   echo '           -a = Install (ALL) Integrations'
   echo '           -u = Update Single/Multiple Integration(s)'
   echo '           -U = Update (ALL) Integrations'
   echo '           -l = List Installed Integration(s)'
   echo '           -r = Remove Single/Multiple Integation(s)'
   echo '           -e = Remove (ALL) Integations'
   echo '           -h = Display This Help'
esac
